package com.blazemeter.ajax.gui;

import com.blazemeter.ajax.AJAXRequest;
import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.config.gui.ArgumentsPanel;
import org.apache.jmeter.samplers.gui.AbstractSamplerGui;
import org.apache.jmeter.testelement.TestElement;
import org.apache.jorphan.gui.ObjectTableModel;
import org.apache.jorphan.reflect.Functor;

import javax.swing.*;
import java.awt.*;


public class AJAXRequestGui extends AbstractSamplerGui {


    private ArgumentsPanel argsPanel;

    public AJAXRequestGui() {
        super();
        init();
    }


    @Override
    public String getLabelResource() {
        return "example_title";
    }

    @Override
    public TestElement createTestElement() {
        AJAXRequest sampler = new AJAXRequest();
        modifyTestElement(sampler);
        return sampler;
    }

    @Override
    public void modifyTestElement(TestElement sampler) {

        super.configureTestElement(sampler);
        AJAXRequest ajaxSampler = (AJAXRequest) sampler;
        ajaxSampler.setArguments((Arguments) argsPanel.createTestElement());

    }

    private void init() {
        setLayout(new BorderLayout());
        setBorder(makeBorder());

        add(makeTitlePanel(), BorderLayout.NORTH);
        add(makeArgumentsPanel(), BorderLayout.CENTER);

        JPanel streamsCodePane = new JPanel(new BorderLayout());
        add(streamsCodePane, BorderLayout.SOUTH);
    }

    private JPanel makeArgumentsPanel() {

        argsPanel = new ArgumentsPanel("AJAX Request Details", null, true, false,  // $NON-NLS-1$
                new ObjectTableModel(new String[]{"Method", "URL", "Value"},
                        Argument.class,
                        new Functor[]{
                                new Functor("getName"), // $NON-NLS-1$
                                new Functor("getValue"),  // $NON-NLS-1$
                                new Functor("getDescription")},  // $NON-NLS-1$
                        new Functor[]{
                                new Functor("setName"), // $NON-NLS-1$
                                new Functor("setValue"), // $NON-NLS-1$
                                new Functor("setDescription")},  // $NON-NLS-1$
                        new Class[]{String.class, String.class, String.class}));
        return argsPanel;

    }

}
